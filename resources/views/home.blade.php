@extends('layouts.app')

@section('content')
<div class="container-fluid bg-white padding-60">

    <div class="row justify-content-center margin-top-20">

        <img id="newImage" width="100%" height="100px" class="hidden">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Publicar nueva imágen
                </div>
                <div class="card-body">
                    <h1>Agregar nueva imágen</h1>

                    <div class="row ">
                        <div class="col border p-4">
                            <h2>
                                Selecciona la imágen que deseas subir 
                                <span class="badge badge-warning" v-show="! isOpencvReady" >
                                    Cargando Opencv
                                </span>
                            </h2>
                            <input  type="file" 
                                    class="form-control-file" 
                                    @change="hideImage()"
                                    id="img"
                                    v-show="isOpencvReady"
                            >
                        </div>
                    </div>
                    
                    <div class="row mt-4">
                        <div class="col border p-4">
                            <h2 class="text-center">Imágen original</h2>
                            <canvas id="canvasInput" 
                                    class="img-fluid mt-4"
                            ></canvas>
                        </div>
                        <div class="col border p-4">
                            <h2 class="text-center">Imágen oculta</h2>
                            <canvas id="canvasOutput" 
                                    class="img-fluid mt-4"
                            ></canvas>
                        </div>
                    </div>

                    <div class="row mt-4">

                        <div class="col text-center">
                            <button class="btn btn-primary text-center"
                                    disabled
                            >
                                Guardar imágen
                            </button>
                        </div>

                    </div>
                    
                </div>
            </div>
        </div>

    </div>

</div>
@endsection

@section('scripts')

    <script src="/js/hide-images.js"></script>

    {{-- La función de opencvReady está definida en hide-images --}}
    <script async src="/js/opencv.js" onload="opencvReady();" type="text/javascript"></script>
    
@endsection
