

const longToByteArray = function(/*long*/n) {
  // we want to represent the input as a 8-bytes array
  let result = [];

  let mask = 255;

  while ( n > 0 ) {
    
    let byte = n & mask;
    result.push(byte);
    n >>= 8;
  }

  return result;

};

const ensureByteSize  = function ( str ) {

	while ( str.length != 8 )
		str = "0" + str;
	
	return str;
}

const toStringArray = function (arr) {
  var result = [ ];

	for ( var i = arr.length - 1; i >= 0; i-- ) {
		
		var binary = arr[ i ].toString( 2 );
		var toAdd = ensureByteSize( binary );
		result.push( toAdd );
	}
  
  if (result.length == 0)
    return ["00000000"]

	return result;
}

function bytesToDouble(str,start) {
  start *= 8;
  var data = [str.charCodeAt(start+7),
              str.charCodeAt(start+6),
              str.charCodeAt(start+5),
              str.charCodeAt(start+4),
              str.charCodeAt(start+3),
              str.charCodeAt(start+2),
              str.charCodeAt(start+1),
              str.charCodeAt(start+0)];

  var sign = (data[0] & 1<<7)>>7;

  var exponent = (((data[0] & 127) << 4) | (data[1]&(15<<4))>>4);

  if(exponent == 0) return 0;
  if(exponent == 0x7ff) return (sign) ? Number.POSITIVE_INFINITY : Number.NEGATIVE_INFINITY;

  var mul = Math.pow(2,exponent - 1023 - 52);
  var mantissa = data[7]+
      data[6]*Math.pow(2,8*1)+
      data[5]*Math.pow(2,8*2)+
      data[4]*Math.pow(2,8*3)+
      data[3]*Math.pow(2,8*4)+
      data[2]*Math.pow(2,8*5)+
      (data[1]&15)*Math.pow(2,8*6)+
      Math.pow(2,52);

  return Math.pow(-1,sign)*mantissa*mul;
}

// function testImage (img) {
//   let src = this.cv.imread(img);

//   let dst = new cv.Mat();

//   let M = cv.Mat.eye(3, 3, cv.CV_32FC1);
//   let anchor = new cv.Point(-1, -1);
//   cv.filter2D(src, dst, cv.CV_8U, M, anchor, 0, cv.BORDER_DEFAULT);
//   cv.imshow('canvasOutput', dst);
//   // src.delete(); dst.delete(); M.delete();
// }


// function addFileInputHandler(fileInputId, canvasId) {
//   let inputElement = document.getElementById(fileInputId);
//   inputElement.addEventListener('change', (e) => {
//       let files = e.target.files;
//       if (files.length > 0) {
//           let imgUrl = URL.createObjectURL(files[0]);
//           self.loadImageToCanvas(imgUrl, canvasId);
//       }
//   }, false);
// };

const app = new Vue({
  el: '#app',
  data() {
    return {
      cv: {},
      newImage: {},
      fakeImage: {}
    }
  },
  computed: {
    isOpencvReady() {
      return Object.keys(this.cv).length > 0
    }
  },
  methods: {

    getMostImportantInfoFromOriginalImage(data) {

      const orgData = { ...data };
      let newData = new Array();

      // console.log(parseInt( a.split('').reverse().join(''), 2 ));

      newData = Object.values(orgData).map( data => {
        return data.toString(2).substring(0,4);
      })

      return newData;

    },

    urltoFile(url, filename, mimeType) {
      return (fetch(url)
        .then(function(res){return res.arrayBuffer();})
        .then(function(buf){return new File([buf], filename, {type:mimeType});})
      );
    },

    hideImageFromImportantBits(importantBits, orgMatrix) {

      let bitsToTake = 4;
      console.log('bits', importantBits);
      if (orgMatrix.isContinuous()) {
        for (let i =0; i < orgMatrix.data.length ; i ++ ) {
          let newValue = orgMatrix.data[i].toString(2).substring(0,4) + importantBits;
          orgMatrix.data[i] = parseInt( newValue.split('').reverse().join(''), 2 );
        }
      }

      return orgMatrix;
    },

    loadImageToCanvas(url, cavansId, isFakeImage = false ) {
      let canvas = document.getElementById(cavansId);
      let ctx = canvas.getContext('2d');
      let img = new Image();
      img.crossOrigin = 'anonymous';
      img.onload = () => {
        
        canvas.width = img.width;
        canvas.height = img.height;

        // Condición para actualizar el scope de la variable fake image
        if ( ! isFakeImage ) {
          
          // Estos son los datos de la matriz original para poder
          // ocultar la imágen
          let mat = this.cv.imread(img);

          this.fakeImage = {
            width: img.width,
            height: img.height,
            data: mat.data
          };

          ctx.drawImage(img, 0, 0, img.width, img.height);
        }
        // Este caso arma la imágen que se va a ocultar
        else {
          const mostImportantBits = this.getMostImportantInfoFromOriginalImage(this.fakeImage.data)
          
          let cv =this.cv;
          let mat = cv.imread(img);

          console.log(mat.data)
          
          mat = this.hideImageFromImportantBits(mostImportantBits, mat)

          // let mat = new cv.Mat();
          // let mat = new cv.Mat(size, type);
          this.cv.imshow('canvasOutput', mat)
          // console.log('Mostimportant',mostImportantBits);
        }        
      
      };
      img.src = url;
      return img;
    },

    hideImage() {
      let files = document.getElementById('img').files;
      const originalImage = this.loadImageToCanvas(URL.createObjectURL(files[0]),'canvasInput')

      let newImage = document.getElementById('newImage');
      newImage.src = URL.createObjectURL(files[0]);
      let mat = this.cv.imread(newImage);

    },
    getImageMatrix(image) {
      let cv = this.cv;

      let files = document.getElementById('img').files;
      
      let rgbaPlanes = new this.cv.MatVector();

      loadImageToCanvas(URL.createObjectURL(files[0]),'canvasInput')


      // this.cv.split(mat, rgbaPlanes);
      
      // let R = rgbaPlanes.get(0);
      // let G = rgbaPlanes.get(1);
      // let B = rgbaPlanes.get(2);

      // this.cv.merge(rgbaPlanes, mat);
      

      // let mat = cv.imread(image);
    }
  },
  watch: {
    newImage(image) {
    },
    fakeImage(fakeImage) {
      const urlToGenerateRandomImages = `https://picsum.photos/${ fakeImage.width }/${ fakeImage.height }`;
      
      this.urltoFile(urlToGenerateRandomImages, 'image.jpg', 'image')
        .then((image)=>{
          this.loadImageToCanvas(URL.createObjectURL(image),'canvasOutput', true);
        });
    }
  },
  mounted() {
  }
});

// Inicializar OpenCV para VueJS
function opencvReady() {
  app.cv = cv;
}
